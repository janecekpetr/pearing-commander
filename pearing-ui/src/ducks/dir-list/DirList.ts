import { Action, ReceivedDirList, DirListEntry, Panel } from 'types/types';

const PREFIX = 'DIR-LIST/';


export const DIR_LIST_REQUESTED = PREFIX + 'DIR_LIST_REQUESTED';
export type DIR_LIST_REQUESTED = { panelId: string, path: string };

export function dirListRequested(panelId: string, path: string): Action<DIR_LIST_REQUESTED> {
	return {
		type: DIR_LIST_REQUESTED,
		payload: {
			panelId,
			path,
		}
	};
}

export const DIR_LIST_RECEIVED = PREFIX + 'DIR_LIST_RECEIVED';
export type DIR_LIST_RECEIVED = { panelId: string, path: string, files: ReceivedDirList, isNewList?: boolean };

export function dirListReceived(panelId: string, path: string, files: ReceivedDirList, isNewList?: boolean): Action<DIR_LIST_RECEIVED> {
	return {
		type: DIR_LIST_RECEIVED,
		payload: {
			panelId,
			path,
			files,
			isNewList,
		}
	};
}

import * as Immutable from 'immutable';

let Panel = Immutable.Record({
	id: 'panel1' as string,
	path: 'defaultValue',
	list: [] as Array<DirListEntry>,
	lastUpdate: Date.now() as number,
});

const initialState = Immutable.fromJS({
	panels: ['panel1'] as Array<string>,
	panelIds: {
		'panel1': {
			id: 'panel1',
			path: 'C:\\',
			list: [],
			lastUpdate: Date.now(),
		}
	}
})


export default function reducer(state = initialState, action: Action<any>) {
	switch (action.type) {
		case DIR_LIST_RECEIVED: {
			let _action = action as Action<DIR_LIST_RECEIVED>

			let panel = state.getIn(['panelIds', _action.payload.panelId]);

			let newPanel = panel;
			if (_action.payload.isNewList) {
				newPanel = newPanel
					.set('path', _action.payload.path)
					.set('list', Immutable.List())
			}

			newPanel = newPanel
				.set('lastUpdate', 0) // TODO: Figure out when to record the date
				.set('list', newPanel.get('list').concat(Immutable.fromJS(_action.payload.files)));

			return state.setIn(['panelIds', _action.payload.panelId], newPanel);
		}

		default: return state;
	}
}


