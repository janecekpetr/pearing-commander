package cz.michalbures.commander.service;

import static com.google.common.base.Preconditions.checkNotNull;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;

import cz.michalbures.commander.Pearing.DirectoryListing;
import cz.michalbures.commander.Pearing.File;
import cz.michalbures.commander.Pearing.File.FileType;
import cz.michalbures.commander.rpc.request.RequestListener;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;

class DirectoryListingListener implements RequestListener<DirectoryListing.Request, DirectoryListing.Response> {
	
	private static final int RESPONSE_CHUNK_SIZE = 256;
	
	private static final Status NOT_A_DIRECTORY = Status.FAILED_PRECONDITION
		.withDescription("The specified file is not a directory.");
	private static final Status UNEXPECTED_ERROR = Status.UNAVAILABLE
		.withDescription("Something bad happened while reading directory contents.");

	private final FileSystem fileSystem;
	private final StreamObserver<DirectoryListing.Response> responseObserver;

	public DirectoryListingListener(FileSystem fileSystem, StreamObserver<DirectoryListing.Response> responseObserver) {
		this.fileSystem = checkNotNull(fileSystem);
		this.responseObserver = checkNotNull(responseObserver);
	}

	@Override
	public StreamObserver<DirectoryListing.Response> respond() {
		return responseObserver;
	}

	@Override
	public void onNext(DirectoryListing.Request request) {
		Path requestedPath = MorePaths.parsePath(fileSystem, request.getDirectoryPath());
		Path directory = checkAccessibleDirectory(requestedPath);
		listFiles(directory);
		respond().onCompleted();
	}
	
	@Override
	public void onCompleted() {
		// no-op
	}
	
	private static Path checkAccessibleDirectory(Path requestedPath) {
		Status directoryAccessStatus = MorePaths.getPathAccessStatus(requestedPath);
		if (!directoryAccessStatus.isOk()) {
			throw directoryAccessStatus.asRuntimeException();
		}
		if (!Files.isDirectory(requestedPath)) {
			throw NOT_A_DIRECTORY.asRuntimeException();
		}
		return requestedPath;
	}

	private void listFiles(Path directory) {
		try (DirectoryStream<Path> files = Files.newDirectoryStream(directory)) {
			Path directoryRealPath = directory.toRealPath();

			// Respond in chunks...
			DirectoryListing.Response.Builder response = DirectoryListing.Response.newBuilder();
			response.setDirectoryPath(directoryRealPath.toString());
			for (Path file : files) {
				response.addFile(pathToFileProto(file));
				// TODO also chunk on timeout
				if (response.getFileCount() == RESPONSE_CHUNK_SIZE) {
					// ...chunk full, respond and reset.
					respond().onNext(response.build());
					response = DirectoryListing.Response.newBuilder();
					response.setDirectoryPath(directoryRealPath.toString());
				}
			}
			// Send the rest if anything is left.
			// This might accidentally send a trailing empty response, but that's allowed (and required for empty dir).
			respond().onNext(response.build());
		} catch (IOException e) {
			throw UNEXPECTED_ERROR.withCause(e).asRuntimeException();
		}
	}

	private static File pathToFileProto(Path file) {
		return File.newBuilder()
			.setName(file.getFileName().toString())
			.setFileType(resolveFileType(file))
			.build();
	}
	
	private static FileType resolveFileType(Path file) {
		if (Files.isRegularFile(file, LinkOption.NOFOLLOW_LINKS)) {
			return FileType.FILE;
		}
		if (Files.isDirectory(file, LinkOption.NOFOLLOW_LINKS)) {
			return FileType.DIRECTORY;
		}
		if (Files.isSymbolicLink(file)) {
			return FileType.SYMBOLIC_LINK;
		}
		return FileType.UNKNOWN;
	}
}