import {applyMiddleware, createStore, Store} from 'redux';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './rootReducer';
import rootSaga from './rootSaga';

import {ElectronReduxCommEnhancer} from 'electron-redux-multi-window-comm';
import {composeWithDevTools} from 'remote-redux-devtools';


export default function configureStore(initialState?:Object):Store<{}>
{
	const composeEnhancers = composeWithDevTools({name: 'PeaRing UI: Electron', realtime: true, hostname: 'localhost', port: 8000});

	const sagaMiddleware = createSagaMiddleware();
	const store          = createStore(
		rootReducer,
		initialState,
		composeEnhancers(
			ElectronReduxCommEnhancer({
				windowName: 'electron',
				debug     : {
					enabled            : true,
					numOfActionsToStore: 20,
				},
			}),
			applyMiddleware(sagaMiddleware)
		)
	);
	
	rootSaga.forEach((saga) =>
	{
		sagaMiddleware.run(saga)
	});

	return store;
}