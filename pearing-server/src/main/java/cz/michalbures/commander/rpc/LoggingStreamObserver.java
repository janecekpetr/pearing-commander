package cz.michalbures.commander.rpc;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.protobuf.Message;
import com.google.protobuf.TextFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.Status;
import io.grpc.Status.Code;
import io.grpc.stub.StreamObserver;

/** A delegate {@link StreamObserver} logging all communication. */
public class LoggingStreamObserver<T extends Message> extends ForwardingStreamObserver<T> {
	
	private final StreamObserver<T> delegate;
	private final Logger logger;
	
	/** Wraps another {@link StreamObserver}, logging the communication flow. */
	public static <T extends Message> StreamObserver<T> wrap(StreamObserver<T> delegate) {
		return new LoggingStreamObserver<>(delegate);
	}

	private LoggingStreamObserver(StreamObserver<T> delegate) {
		this.delegate = checkNotNull(delegate);
		this.logger = LoggerFactory.getLogger(delegate.getClass());
	}

	@Override
	protected StreamObserver<T> delegate() {
		return delegate;
	}

	@Override
	public void onNext(T request) {
		if (logger.isInfoEnabled()) {
			logger.info("Message on stream: {}{{}}",
					request.getDescriptorForType().getName(), TextFormat.shortDebugString(request));
		}
		delegate.onNext(request);
	}
	
	@Override
	public void onCompleted() {
		logger.info("Call completed.");
		delegate.onCompleted();
	}
	
	@Override
	public void onError(Throwable t) {
		Status status = Status.fromThrowable(t);
		if (status.getCode() == Code.CANCELLED) {
			logger.info("Call cancelled: {}", status);
		} else {
			logger.error("Call error, aborting!", t);
		}
		delegate.onError(t);
	}
	
}