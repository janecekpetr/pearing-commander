import {BrowserWindow} from 'electron';

import {APP_FOLDER} from 'main/variables/root'

export const name:string = 'app-window';

export const config = {
	url    : 'file://' + APP_FOLDER + '/app/app-window/index.html',
	options: {
		width : 1130,
		height: 860,
		show  : true,
		webPreferences: {experimentalFeatures: true},
	},
	setup  : function (window: Electron.BrowserWindow)
	{
		// window.webContents.openDevTools({mode: 'bottom'})
	}
};