import * as React from 'react';
import { Store } from 'react-redux';
import { Task , SagaMiddleware, SagaIterator} from 'redux-saga';

export interface ISagaRunnerProps {
	running: boolean;
	saga: () => IterableIterator<any>;
}

export interface ISagaRunnerState {
	sagaDescriptor: object;
}

export interface ISagaRunnerContext {
	store: ISagaRunnerStore;
}



export interface ISagaRunnerStore extends Store{
	runSaga: (saga:() => SagaIterator, prop1?:object, prop2?:object) => Task;
}

export default class SagaRunner extends React.Component<ISagaRunnerProps, ISagaRunnerState> {
	context: ISagaRunnerContext;
	
	
	static contextTypes = {
		store: React.PropTypes.object.isRequired
	};
	
	sagaDescriptor: Task = null;
	
	getProps = () => {
		return this.props;
	}
	
	
	constructor(props, context)
	{
		super(props, context);
		console.log('SAGA RUNNER 0000')
		console.log(props, context)

		// if (this.props.running && this.props.saga) {
		// 	console.log('start')
			
		// 	this.sagaDescriptor = this.context.store.runSaga(this.props.saga, this.props, this.getProps)
		// 	console.log(this.sagaDescriptor)
		// }

	}
	
	componentDidMount() {
		
		// Start saga
		if (this.props.running && this.props.saga) {
			console.log('start')
			
			this.sagaDescriptor = this.context.store.runSaga(this.props.saga, this.props, this.getProps)
			console.log(this.sagaDescriptor)
		}
	}
	

	componentWillReceiveProps(nextProps) {
		console.log('SAGA RUNNER 111')
		console.log(nextProps)
		
		// Enable saga
		if (this.props.running === false && nextProps.running === true) {
			this.sagaDescriptor = this.context.store.runSaga(nextProps.saga, nextProps, this.getProps)
			return;
		}
		
		// Disable saga
		if (this.props.running === true && nextProps.running === false)
		{
			this.sagaDescriptor.cancel();
			return;
		}


		// Enable/don't stop new saga
		if (nextProps.running && this.props.saga !== nextProps.saga) {
			console.log('Different saga passed')
			
			this.sagaDescriptor.cancel();
			this.sagaDescriptor = this.context.store.runSaga(nextProps.saga, nextProps, this.getProps)
			return;
		}
	}

	componentWillUnmount() {
		if (this.sagaDescriptor) {
			this.sagaDescriptor.cancel();
		}
	}

	render()
	{
		return null;
	}
}