import { channel, Channel } from 'redux-saga';
import { take, put, race, call, cps, fork, join, cancel, cancelled, select } from 'redux-saga/effects';

import grpc from 'grpc';

import { Actions as GrpcActions } from 'ducks/grpc';
import { DIR_LIST_REQUESTED, dirListReceived } from './DirList';
import { Action, GrpcCall } from 'types/types';
import { getFileServiceStub, connectGrpcCallToChannel } from './DirListGrpc';



function* listenForDirListRequest() {
	while (true) {
		const action = yield take(DIR_LIST_REQUESTED);
		yield fork(dirList, action)
	}
}



function* dirList(action:Action<DIR_LIST_REQUESTED>): IterableIterator<any> {
	const listDirChannel = channel();
	let stub = getFileServiceStub();


	const  {path, panelId} = action.payload;

	let call:GrpcCall = stub.listDirectory();
	call = connectGrpcCallToChannel(listDirChannel, call);

	call.write({directory_path: path});

	let firstData:boolean = true;
	while (true) {
		const action = yield take(listDirChannel);

		switch(action.type) {
			case GrpcActions.GRPC_DATA: {
				const {file} = action.payload
				yield put(dirListReceived(panelId, path, file, firstData))
				firstData = false;
			}
		}

		if (action.type === GrpcActions.GRPC_END) {
			break;
		}
	}
}



export default function* rootSaga(props?, getProps?): IterableIterator<any> {
	yield listenForDirListRequest();
}