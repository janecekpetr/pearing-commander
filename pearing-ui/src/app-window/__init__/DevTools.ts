if (process.env.NODE_ENV !== 'production')
{
	var Immutable = require("immutable");

	var installDevTools = require("immutable-devtools");
	installDevTools(Immutable);
}