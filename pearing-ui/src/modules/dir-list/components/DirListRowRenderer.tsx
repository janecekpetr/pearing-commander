import * as React from 'react';
import { RowRendererParams } from 'types/react-virtualized';


export default function defaultRowRendererWrapper(isSelected) {
	return function defaultRowRenderer({
		className,
		columns,
		index,
		isScrolling,
		key,
		onRowClick,
		onRowDoubleClick,
		onRowMouseOver,
		onRowMouseOut,
		rowData,
		style,
}: RowRendererParams) {
		const a11yProps: any = {}

		if (
			onRowClick ||
			onRowDoubleClick ||
			onRowMouseOver ||
			onRowMouseOut
		) {
			a11yProps['aria-label'] = 'row'
			a11yProps.tabIndex = 0

			if (onRowClick) {
				a11yProps.onClick = (event) => onRowClick({ event, index, rowData })
			}
			if (onRowDoubleClick) {
				a11yProps.onDoubleClick = (event) => onRowDoubleClick({ event, index, rowData })
			}
			if (onRowMouseOut) {
				a11yProps.onMouseOut = (event) => onRowMouseOut({ event, index, rowData })
			}
			if (onRowMouseOver) {
				a11yProps.onMouseOver = (event) => onRowMouseOver({ event, index, rowData })
			}
		}

		let selected = '';
		if (isSelected(rowData.get('name'))) {
			if (!style) {
				style = {}
			}

			style.backgroundColor = '#6996FFD1';
		}

		style.cursor = 'pointer';

		return (
			<div
				{...a11yProps}
				className={className}
				key={key}
				role='row'
				style={style}
			>
				{columns}
			</div>
		)
	}
}
