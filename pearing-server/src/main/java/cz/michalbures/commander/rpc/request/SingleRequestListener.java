package cz.michalbures.commander.rpc.request;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.protobuf.Message;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;

/** A delegate {@link RequestListener} asserting correct communication flow of cancellable unary RPC calls. */
public class SingleRequestListener<RequestT extends Message, ResponseT extends Message>
		extends ForwardingRequestListener<RequestT, ResponseT> {
	
	private static final Status TOO_FEW_REQUESTS = Status.INVALID_ARGUMENT
		.withDescription("No requests were made for a cancellable unary RPC call.");
	private static final Status TOO_MANY_REQUESTS = Status.INVALID_ARGUMENT
		.withDescription("Too many requests were made for a cancellable unary RPC call.");

	private final RequestListener<RequestT, ResponseT> delegate;
	
	private boolean requestSeen = false;
	
	/**
	 * Wraps another {@link RequestListener}, logging and asserting correct communication flow:
	 * <ul>
	 *     <li>its {@link #onNext(Message)} method won't be called more than once.</li>
	 *     <li>its {@link #onCompleted()} method will only be called after a request has been made.</li>
	 * </ul>
	 */
	public static <RequestT extends Message, ResponseT extends Message> StreamObserver<RequestT>
			wrap(RequestListener<RequestT, ResponseT> delegate) {
		RequestListener<RequestT, ResponseT> singleRequestListener = new SingleRequestListener<>(delegate);
		return ErrorHandlingRequestListener.wrap(singleRequestListener);
	}

	private SingleRequestListener(RequestListener<RequestT, ResponseT> delegate) {
		this.delegate = checkNotNull(delegate);
	}

	@Override
	protected RequestListener<RequestT, ResponseT> delegate() {
		return delegate;
	}

	@Override
	public void onNext(RequestT request) {
		if (requestSeen) {
			throw TOO_MANY_REQUESTS.asRuntimeException();
		}

		requestSeen = true;
		delegate.onNext(request);
	}
	
	@Override
	public void onCompleted() {
		if (!requestSeen) {
			throw TOO_FEW_REQUESTS.asRuntimeException();
		}
		delegate.onCompleted();
	}
	
}