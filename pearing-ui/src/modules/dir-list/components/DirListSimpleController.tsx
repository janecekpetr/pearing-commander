import * as React from 'react';
import { Provider, connect } from 'react-redux';
import {dirListRequested} from 'ducks/dir-list/DirList';

import {
	Button,
	Menu,
	MenuItem,
	MenuDivider,
	Popover,
	Position,
	InputGroup,
} from "@blueprintjs/core";

class DirListSimpleContollerClass extends React.Component<any, any> {

	textInput: HTMLInputElement;

	constructor(props, context) {
		super(props, context);
	}

	resetFolder = () => {
		this.props.dispatch(dirListRequested('panel1', 'D:\\Temps\\Windows\\Temp\\'))
	}

	goToFolder = () => {
		const path = this.textInput.value;

		if (!path) {
			return;
		}

		this.props.dispatch(dirListRequested('panel1', path))
	}


	render() {
		return (
			<div>
				<Button onClick={this.resetFolder}>Reset</Button>
				<Button onClick={this.goToFolder}>Go to: </Button>
				<InputGroup
					inputRef={(input) => { this.textInput = input; }}
					type="text"
					defaultValue={'D:\\Temps\\Windows\\Temp\\'}
				/>
				
			</div>
		)
	}
}

export const DirListSimpleController = connect()(DirListSimpleContollerClass)