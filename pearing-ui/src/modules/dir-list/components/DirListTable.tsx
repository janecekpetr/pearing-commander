import * as React from 'react';
import * as Immutable from 'immutable';
import { join, resolve } from 'path';


import { connect } from 'react-redux';
import { Column, Table, SortDirection, AutoSizer } from 'react-virtualized';
import { ISortDirection } from 'types/react-virtualized';
import { FILE_TYPES } from 'modules/constants';
import { dirListRequested } from 'ducks/dir-list/DirList';


import DirListRowRenderer from './DirListRowRenderer';


interface IDirListTableProps {
	type?: string;
	name?: string;
	path?: string;
	dispatch?: any;
	list?: Immutable.List<any>;
}

interface IDirListTableState {
	sortBy?: string;
	sortDirection?: ISortDirection;
	selectedRows?: Array<string>;
	dispatch?: any;
	list?: Immutable.List<any>;
}

class DirListTable extends React.Component<IDirListTableProps, IDirListTableState> {

	constructor(props, context) {
		super(props, context);

		this.state = {
			sortBy: 'index',
			sortDirection: SortDirection.DESC,
			selectedRows: [],
			list: this.props.list,
		}
	}

	componentWillReceiveProps = (nextProps: IDirListTableProps) => {
		if (this.props.list !== nextProps.list) {
			const list: Immutable.List<any> = nextProps.list;

			this.setState({
				list: sortTable(list, this.state.sortBy as string, this.state.sortDirection as ISortDirection)
			})
		}
	}


	onRowClick = ({ event, index, rowData }) => {
		let selectedRows: Array<any> = this.state.selectedRows;


		// CTRL + CLICK
		// Select or unselect many rows one by one
		if (event.ctrlKey) {
			const selectedRowIndex = this.state.selectedRows.indexOf(rowData.get('name'))

			if (selectedRowIndex >= 0) {
				// Unselect already selected row
				selectedRows.splice(selectedRowIndex, 1)
			} else {
				selectedRows.push(rowData.get('name'))
			}

			this.setState({ selectedRows })
			return;

		} else if (event.altKey) {

			// SHIFT + CLICK
			// Select all rows from the currently selected row or from the beginning
		} else if (event.shiftKey) {
			let startIndex = 0;
			const list = this.state.list
			if (selectedRows.length > 0) {
				// First selected row ever in this select session
				startIndex = list.findKey((value) => value.get('name') === selectedRows[0])
			}

			// Select the rows
			selectedRows = [];
			for (var rowIndex = startIndex; rowIndex <= index; rowIndex++) {
				selectedRows.push(list.getIn([rowIndex, 'name']));
			}

			this.setState({ selectedRows })

			return;
		}


		// SIMPLE CLICK

		selectedRows = [];

		// Select the clicked row
		if (this.state.selectedRows.indexOf(rowData.get('name')) < 0) {
			selectedRows.push(rowData.get('name'));
		}

		// If there are multiple selected rows (such as from shift+click)
		// then select only the clicked one even if it's already selected
		if (this.state.selectedRows.length > 1) {
			selectedRows.push(rowData.get('name'));
		}

		this.setState({ selectedRows })

	}

	onRowDoubleClick = ({ event, index, rowData }) => {

		// Open folder
		if (rowData.get('type') === FILE_TYPES.DIRECTORY) {
			const path = join(this.props.path, rowData.get('name'))
			console.log(path);
			this.props.dispatch(dirListRequested('panel1', path))
			return;
		}

		// Special case '..' - go up
		if (index === 0) {
			const path = resolve(this.props.path, '..')
			console.log(path);
			this.props.dispatch(dirListRequested('panel1', path))
		}

	}

	isRowSelected = (index) => {
		const selectedRowIndex = this.state.selectedRows.indexOf(index)
		return selectedRowIndex >= 0;
	}

	sortTable = ({ sortBy, sortDirection }: { sortBy: string, sortDirection: ISortDirection }) => {
		const sortOrder = this.state.sortDirection;
		const newSortDirection = sortOrder === SortDirection.ASC ? SortDirection.DESC : SortDirection.ASC;

		const sortedList = sortTable(this.state.list, '', newSortDirection)

		this.setState({ sortBy, sortDirection: newSortDirection, list: sortedList })
	}

	render() {
		const { list } = this.props;
		const sortOrder = this.state.sortDirection;
		const stateList = this.state.list;

		return (
			<div style={{ flex: '1' }}>
				<AutoSizer>
					{({ height, width }) => (
						<Table
							width={width}
							height={height}
							headerHeight={20}
							rowHeight={({ index }) => 20}
							rowCount={stateList.count() + 1}
							rowGetter={({ index }) => {
								if (index === 0) {
									return Immutable.Map({ name: '..' })
								}
								return stateList.get(index - 1)
							}
							}
							sort={this.sortTable}
							sortDirection={sortOrder}
							sortBy={'name'}
							onRowClick={this.onRowClick}
							onRowDoubleClick={this.onRowDoubleClick}
							rowRenderer={DirListRowRenderer(this.isRowSelected)}
						>
							<Column
								label='type'
								dataKey='type'
								width={18}
								cellRenderer={renderType}
							/>
							<Column
								label='Name'
								dataKey='name'
							/>
						</Table>
					)}
				</AutoSizer>
			</div>
		);
	}
}

export default DirListTable;


function renderType({ cellData: type, rowData }: {
	cellData: any,
	columnData: any,
	dataKey: string,
	isScrolling: boolean,
	rowData: any,
	rowIndex: number
}) {
	let fileTypeClass = type === FILE_TYPES.DIRECTORY ? 'pt-icon-folder-close' : 'pt-icon-document';
	const fileTypeColor = type === FILE_TYPES.DIRECTORY ? '#e8c301' : '#848484';

	if (rowData.get('name') === '..') {
		fileTypeClass = 'pt-icon-arrow-up';
	}

	return <span
		style={{ color: fileTypeColor }}
		className={'pt-icon-standard ' + fileTypeClass}
	></span>
}

function sortTable(list: Immutable.List<any>, sortBy: string, sortDirection: ISortDirection): Immutable.List<any> {
	const sortedList = list.sort((a, b) => {
			let order = 1;
			if (sortDirection === SortDirection.ASC) {
				order = -1;
			}

			const typeA = a.get('type');
			const typeB = b.get('type');

			// Sort always directories first

			if (typeA === FILE_TYPES.DIRECTORY && typeB === FILE_TYPES.DIRECTORY) {
				return order * a.get('name').localeCompare(b.get('name'))
			} else if (typeA === FILE_TYPES.DIRECTORY) {
				return -1;
			} else if (typeB === FILE_TYPES.DIRECTORY) {
				return 1;
			} else {
				return order * a.get('name').localeCompare(b.get('name'))

			}

		}) as Immutable.List<any>;


	return sortedList;
}