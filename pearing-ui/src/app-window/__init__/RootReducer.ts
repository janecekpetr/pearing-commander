import {ElectronReduxCommReducer} from 'electron-redux-multi-window-comm';


// Add 3rd party reducers
const thirdPartyReducers = {
	ElectronReduxComm: ElectronReduxCommReducer,

};

import DirListReducer from 'ducks/dir-list/DirList';

const RootReducer = {
	DirList: DirListReducer,
}

import Paradux from '../../lib/paradux';

export const paradux = new Paradux({...RootReducer, ...thirdPartyReducers});

export default paradux.reducerWrapper()