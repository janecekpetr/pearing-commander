import {ElectronReduxCommSaga} from 'electron-redux-multi-window-comm';
import {SimpleWindowManagerSaga} from 'electron-simple-window-manager';


export default [
	ElectronReduxCommSaga,
	SimpleWindowManagerSaga,
];