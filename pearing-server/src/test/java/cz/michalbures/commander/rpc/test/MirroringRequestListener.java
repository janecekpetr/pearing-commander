package cz.michalbures.commander.rpc.test;

import cz.michalbures.commander.rpc.request.RequestListener;

import io.grpc.stub.StreamObserver;
import io.grpc.testing.protobuf.SimpleRequest;
import io.grpc.testing.protobuf.SimpleResponse;

/** A testing {@link RequestListener} that mirrors its input by sending the same response right away. */
public class MirroringRequestListener implements RequestListener<SimpleRequest, SimpleResponse> {

	private final StreamObserver<SimpleResponse> responseObserver;

	public MirroringRequestListener(StreamObserver<SimpleResponse> responseObserver) {
		this.responseObserver = responseObserver;
	}

	@Override
	public StreamObserver<SimpleResponse> respond() {
		return responseObserver;
	}

	@Override
	public void onNext(SimpleRequest request) {
		SimpleResponse response = SimpleResponse.newBuilder()
			.setResponseMessage(request.getRequestMessage())
			.build();
		respond().onNext(response);
	}

	@Override
	public void onCompleted() {
		respond().onCompleted();
	}
}