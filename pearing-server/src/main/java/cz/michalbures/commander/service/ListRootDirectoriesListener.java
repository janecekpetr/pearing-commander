package cz.michalbures.commander.service;

import cz.michalbures.commander.Pearing.ListRootDirectoriesRequest;
import cz.michalbures.commander.Pearing.RootDirectory;
import cz.michalbures.commander.rpc.request.RequestListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import io.grpc.stub.StreamObserver;
import oshi.SystemInfo;
import oshi.software.os.OSFileStore;
import oshi.software.os.OperatingSystem;

class ListRootDirectoriesListener implements RequestListener<ListRootDirectoriesRequest, RootDirectory> {
	
	private static final Logger logger = LoggerFactory.getLogger(ListRootDirectoriesListener.class);
	private static final OperatingSystem OPERATING_SYSTEM = new SystemInfo().getOperatingSystem();
	
	private final StreamObserver<RootDirectory> responseObserver;

	public ListRootDirectoriesListener(StreamObserver<RootDirectory> responseObserver) {
		this.responseObserver = responseObserver;
	}

	@Override
	public StreamObserver<RootDirectory> respond() {
		return responseObserver;
	}
	
	@Override
	public void onNext(ListRootDirectoriesRequest request) {
		OSFileStore[] fileStores = OPERATING_SYSTEM.getFileSystem().getFileStores();
		for (OSFileStore fileStore : fileStores) {
			logger.info("File store - name: {}, desc: {}, mount: {}, type: {}",
					fileStore.getName(), fileStore.getDescription(), fileStore.getMount(), fileStore.getType());
		}
		
		respond().onCompleted();
	}

	@Override
	public void onCompleted() {
		// no-op
	}
}