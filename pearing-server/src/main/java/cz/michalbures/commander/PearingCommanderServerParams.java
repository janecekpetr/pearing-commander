package cz.michalbures.commander;

import com.beust.jcommander.Parameter;

class PearingCommanderServerParams {
	
	@Parameter(
			names = { "-p", "--port" },
			required = true,
			description = "Port for the Pearing Server to communicate on."
	)
	private int port;

	public int getPort() {
		return port;
	}

}