package cz.michalbures.commander.service;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

import java.nio.file.FileSystem;

import cz.michalbures.commander.FileServiceGrpc.FileServiceImplBase;
import cz.michalbures.commander.Pearing.DirectoryListing;
import cz.michalbures.commander.Pearing.FileTransfer;
import cz.michalbures.commander.Pearing.ListRootDirectoriesRequest;
import cz.michalbures.commander.Pearing.RootDirectory;
import cz.michalbures.commander.rpc.LoggingStreamObserver;
import cz.michalbures.commander.rpc.request.SingleRequestListener;

import io.grpc.stub.StreamObserver;

public class FileService extends FileServiceImplBase {
	
	private final FileSystem fileSystem;

	public FileService(FileSystem fileSystem) {
		checkNotNull(fileSystem);
		checkArgument(fileSystem.isOpen(), "The provided filesystem is closed: %s", fileSystem);
		
		this.fileSystem = fileSystem;
	}
	
	@Override
	public StreamObserver<DirectoryListing.Request> listDirectory(StreamObserver<DirectoryListing.Response> responseObserver) {
		StreamObserver<DirectoryListing.Response> loggingResponseObserver = LoggingStreamObserver.wrap(responseObserver);
		DirectoryListingListener fileRequestListener = new DirectoryListingListener(fileSystem, loggingResponseObserver);
		StreamObserver<DirectoryListing.Request> streamObserver = SingleRequestListener.wrap(fileRequestListener);
		return LoggingStreamObserver.wrap(streamObserver);
	}
	
	@Override
	public StreamObserver<FileTransfer.Request> transferFiles(StreamObserver<FileTransfer.Response> responseObserver) {
		StreamObserver<FileTransfer.Response> loggingResponseObserver = LoggingStreamObserver.wrap(responseObserver);
		FileTransferListener transferRequestListener = new FileTransferListener(fileSystem, loggingResponseObserver);
		StreamObserver<FileTransfer.Request> streamObserver = SingleRequestListener.wrap(transferRequestListener);
		return LoggingStreamObserver.wrap(streamObserver);
	}
	
	@Override
	public StreamObserver<ListRootDirectoriesRequest> listRootDirectories(StreamObserver<RootDirectory> responseObserver) {
		StreamObserver<RootDirectory> loggingResponseObserver = LoggingStreamObserver.wrap(responseObserver);
		
		ListRootDirectoriesListener fileRequestListener = new ListRootDirectoriesListener(loggingResponseObserver);
		StreamObserver<ListRootDirectoriesRequest> singleRequestListenerAsObserver = SingleRequestListener.wrap(fileRequestListener);
		
		return LoggingStreamObserver.wrap(singleRequestListenerAsObserver);
	}

}