import * as React from 'react';
import { Store } from 'react-redux';
import { Task, SagaMiddleware, SagaIterator } from 'redux-saga';

import { paradux } from './../app-window/__init__/RootReducer';

export interface IReducerComponentProps {
	reducer: (state, action) => object;
}

export interface IReducerComponentState {
	removeReducer: Function;
}



export default class ReducerComponent extends React.Component<IReducerComponentProps, IReducerComponentState> {
	removeReducer: ()=>void;


	constructor(props, context) {
		super(props, context);
	}

	componentDidMount() {

		// Start saga
		if (this.props.reducer) {
			console.log('start reducer')

			this.removeReducer = paradux.register(this.props.reducer)
		}
	}

	componentWillUnmount() {
		if (this.removeReducer) {
			this.removeReducer = paradux.register(this.props.reducer)
		}
	}


	render() {
		return null;
	}
}