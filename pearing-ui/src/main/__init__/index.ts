import { setStore } from 'lib/utils/storeReference'

import configureStore from './configureStore';
import configureElectron from './configureElectron';

import initSimpleWindowManager from 'electron-simple-window-manager';
import { openWindow } from 'electron-simple-window-manager/actions';
import windows from 'main/windows/index';

import { app } from 'electron';

app.on('ready', function () {
	initSimpleWindowManager(windows);


	const store = configureStore();
	setStore(store);

	configureElectron();

		store.dispatch(openWindow({windowName: 'app-window'}));
});

