package cz.michalbures.commander.rpc.request;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.concurrent.TimeUnit;

import cz.michalbures.commander.rpc.test.MirroringRequestListener;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import io.grpc.Status;
import io.grpc.Status.Code;
import io.grpc.stub.StreamObserver;
import io.grpc.testing.GrpcServerRule;
import io.grpc.testing.StreamRecorder;
import io.grpc.testing.protobuf.SimpleRequest;
import io.grpc.testing.protobuf.SimpleResponse;
import io.grpc.testing.protobuf.SimpleServiceGrpc;
import io.grpc.testing.protobuf.SimpleServiceGrpc.SimpleServiceImplBase;
import io.grpc.testing.protobuf.SimpleServiceGrpc.SimpleServiceStub;

public class SingleRequestListenerTest {
	
	private static class TestService extends SimpleServiceImplBase {
		@Override
		public StreamObserver<SimpleRequest> bidiStreamingRpc(StreamObserver<SimpleResponse> responseStream) {
			return SingleRequestListener.wrap(new MirroringRequestListener(responseStream));
		}
	}

	@Rule
	public final Timeout timeout = new Timeout(1, TimeUnit.SECONDS);
	@Rule
	public final GrpcServerRule grpcServerRule = new GrpcServerRule();

	private SimpleServiceStub testService;
	private final StreamRecorder<SimpleResponse> responseRecorder = StreamRecorder.create();

	@Before
	public void before() {
		grpcServerRule.getServiceRegistry()
			.addService(new TestService());
		testService = SimpleServiceGrpc.newStub(grpcServerRule.getChannel());
	}
	
	@Test
	public void whenOneRequestSent_completeCallSuccessfully() throws Exception {
		StreamObserver<SimpleRequest> fullDuplexCall = testService.bidiStreamingRpc(responseRecorder);
		fullDuplexCall.onNext(SimpleRequest.getDefaultInstance());
		fullDuplexCall.onCompleted();

		assertResponses(1);
	}

	@Test
	public void whenTwoRequestsSent_errorCodeInvalidArgument() throws Exception {
		StreamObserver<SimpleRequest> fullDuplexCall = testService.bidiStreamingRpc(responseRecorder);
		fullDuplexCall.onNext(SimpleRequest.getDefaultInstance());
		fullDuplexCall.onNext(SimpleRequest.getDefaultInstance());

		assertErrorCode(Code.INVALID_ARGUMENT);
	}
	
	@Test
	public void whenNoRequestsSent_errorCodeInvalidArgument() throws Exception {
		StreamObserver<SimpleRequest> fullDuplexCall = testService.bidiStreamingRpc(responseRecorder);
		fullDuplexCall.onCompleted();

		assertErrorCode(Code.INVALID_ARGUMENT);
	}
	
	@Test
	public void whenClientCancels_errorCodeCancelled() throws Exception {
		StreamObserver<SimpleRequest> fullDuplexCall = testService.bidiStreamingRpc(responseRecorder);
		fullDuplexCall.onError(Status.CANCELLED.asException());

		assertErrorCode(Code.CANCELLED);
	}
	
	@Test
	public void whenClientCancelsAfterOneMessage_errorCodeCancelled() throws Exception {
		StreamObserver<SimpleRequest> fullDuplexCall = testService.bidiStreamingRpc(responseRecorder);
		fullDuplexCall.onNext(SimpleRequest.getDefaultInstance());
		fullDuplexCall.onError(Status.CANCELLED.asException());

		assertErrorCode(Code.CANCELLED);
	}
	
	private void assertResponses(int responsesCount) throws Exception {
		responseRecorder.awaitCompletion();
		assertThat(responseRecorder.getValues()).hasSize(responsesCount);
	}
	
	private void assertErrorCode(Code code) throws Exception {
		responseRecorder.awaitCompletion();
		Throwable error = responseRecorder.getError();
		assertThat(error).isNotNull();
		assertThat(Status.fromThrowable(error).getCode()).isEqualTo(code);
	}
}