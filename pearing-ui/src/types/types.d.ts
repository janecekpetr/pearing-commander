export interface Action<P> {
	type: string;
	payload: P;
	error?: boolean;
	meta?: Object;
}


export interface DirListEntry {
	type: string;
	name: string;
}


export type ReceivedDirList = Array<string>


export interface Panel {
	id: string;
	path: string;
	list: Array<DirListEntry>,
	lastUpdate: number,
}


export interface GrpcCall {
	on(event: string, listener: Function): void;
	on(event: "data", listener: (data: object) => void): void;
	on(event: "status", listener: (status: object) => void): void;
	on(event: "error", listener: (error: object) => void): void;
	on(event: "end", listener: () => void): void;

	end():void;
	write(data:object):void;
}