import { app } from 'electron';
import { resolve } from 'path';
import {type} from 'process';

export function getProtoPath(proto:string) {

	let appPath = '';

	if (type === 'renderer') {
		let app = require('electron').remote.app;
		appPath = app.getAppPath();
	} else if (type === 'browser') {
		let app = require('electron').app;
		appPath = app.getAppPath();
	}
	
	return resolve(appPath, '..', 'pearing-api/src/main/protobuf', proto);
}