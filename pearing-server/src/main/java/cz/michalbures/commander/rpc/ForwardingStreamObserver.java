package cz.michalbures.commander.rpc;

import com.google.common.collect.ForwardingObject;
import com.google.protobuf.Message;

import io.grpc.stub.StreamObserver;

abstract class ForwardingStreamObserver<RequestT extends Message>
		extends ForwardingObject implements StreamObserver<RequestT> {
	
	@Override
	abstract protected StreamObserver<RequestT> delegate();

	@Override
	public void onNext(RequestT value) {
		delegate().onNext(value);
	}

	@Override
	public void onError(Throwable t) {
		delegate().onError(t);
	}

	@Override
	public void onCompleted() {
		delegate().onCompleted();
	}

}