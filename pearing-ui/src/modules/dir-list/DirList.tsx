import * as React from 'react';
import { connect } from 'react-redux';
import * as Immutable from 'immutable';
import { resolve } from 'path';
import DirListTable from './components/DirListTable';
import {dirListRequested} from 'ducks/dir-list/DirList';


interface IDirListProps {
	path?: string;
	files?: any;
	dispatch?: any;
	dirList: Immutable.Map<any, any>;
}


class DirList extends React.Component<IDirListProps, void> {

    constructor(props, context) {
        super(props, context);
    }

	componentDidMount() {
		this.props.dispatch(dirListRequested('panel1', 'C:\\'));
	}
	

    up = () => {
		this.props.dispatch(dirListRequested('panel1', resolve(this.props.dirList.get('path'), '..')))
	}


    render() {
        const dirList = this.props.dirList;
		const path: string = dirList.get('path');
		const list = dirList.get('list');

        return (
            <div style={{ border: '1px solid #232323', display: 'flex', flexDirection: 'column', flex: '1'}}>
                <div style={{ padding: '4px 2px', backgroundColor: '#fafafa' }}>
					<span onClick={this.up} style={{ float: 'right', cursor: 'pointer' }} className={'pt-icon-standard ' + 'pt-icon-arrow-up'}></span>
					<strong>{path}</strong>
				</div>
				<DirListTable
					list={list}
					path={path}
					dispatch={this.props.dispatch}
				/>
            </div>
        );
    }
}


export default connect((state, ownProps) => {
	if (state.DirList && state.DirList.hasIn(['panelIds', 'panel1'])) {
		return {
			dirList: state.DirList.getIn(['panelIds', 'panel1']),
		}
	}
	return {
		dirList: Immutable.Map()
	}

})(DirList)