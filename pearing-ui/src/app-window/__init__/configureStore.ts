import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import RootReducer from './RootReducer';
import './DevTools';
import RootSaga from './RootSaga';
import { ElectronReduxCommEnhancer } from 'electron-redux-multi-window-comm';

import { composeWithDevTools } from 'remote-redux-devtools';
import { ISagaRunnerStore } from '../../lib/SagaRunner';


export default function configureStore(initialState?: Object):ISagaRunnerStore {

	const composeEnhancers = composeWithDevTools({ name: 'PeaRing UI', realtime: true, port: 8000 });

	const sagaMiddleware = createSagaMiddleware();
	let store:any = createStore(
		RootReducer,
		initialState,
		composeEnhancers(
			ElectronReduxCommEnhancer({
				windowName: 'main',
				debug: {
					enabled: true,
					numOfActionsToStore: 20,
				},
				subscribeTo: []
			}),
			applyMiddleware(sagaMiddleware),
		)
	);


	RootSaga.forEach((saga) => {
		sagaMiddleware.run(saga)
	});

	store.runSaga = sagaMiddleware.run;


	return store;
}