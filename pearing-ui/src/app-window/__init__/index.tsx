import * as React from 'react';
import { render } from 'react-dom';
import Root from './Root';

window.location.hash = '/';

render(
	<Root />,
	document.getElementById('MainContent')
);
