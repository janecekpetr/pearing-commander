import { channel, Channel } from 'redux-saga';
import { GrpcCall } from 'types/types';
import { Actions } from 'ducks/grpc';

import grpc from 'grpc';

import { getProtoPath } from './utils';

export function getFileServiceStub() {
	let stub = null;
	
	try {
		let protoPath = getProtoPath('Pearing.proto');

		let protoDescriptor = null;

		// Temporarily handle proto load in both dev and master
		try {
			protoDescriptor = grpc.load('F:\\Programming\\Projects\\PeaRing\\pearing-commander\\pearing-api\\src\\main\\protobuf\\Pearing.proto');
		} catch (e) {
			protoDescriptor = grpc.load(protoPath);
		}
		
		let FileService = protoDescriptor.cz.michalbures.commander.FileService;
		stub = new FileService('localhost:3034', grpc.credentials.createInsecure())
	} catch (e) {
		console.log('GRPC CONNECT ERROR');
		console.log(e);
	}

	return stub;
}


export function connectGrpcCallToChannel(channel:Channel<any>, call:GrpcCall) {
        call.on('data', function (data)
		{
			console.log('DATA');
			console.log(data);
			channel.put({type: Actions.GRPC_DATA, payload: data})
		});
		call.on('end', function ()
		{
			console.log('END');
			channel.put({type: Actions.GRPC_END})
		});
		call.on('status', function (status)
		{
			console.log('STATUS');
			console.log(status);
			channel.put({type: Actions.GRPC_STATUS})
			
		});

		call.on('error', function (error)
		{
			console.log('ERROR');
			console.log(error);
			channel.put({type: Actions.GRPC_ERROR})
		});

		
		return call;
}