package cz.michalbures.commander;

import java.io.IOException;
import java.nio.file.FileSystems;

import cz.michalbures.commander.service.FileService;

import io.grpc.Server;
import io.grpc.ServerBuilder;

public class PearingCommanderServer {

	private final Server server;

	public PearingCommanderServer(int port) {
		server = ServerBuilder.forPort(port)
			.addService(new FileService(FileSystems.getDefault()))
			.directExecutor()
			.build();
	}

	public void start() throws IOException {
		server.start();
		try {
			server.awaitTermination();
		} catch (InterruptedException ignored) {
			Thread.currentThread().interrupt();
		} finally {
			if (!server.isShutdown()) {
				shutdown();
			}
		}
	}
	
	public void shutdown() {
		server.shutdown();
	}
	
}