package cz.michalbures.commander.rpc.request;

import com.google.common.collect.ForwardingObject;
import com.google.protobuf.Message;

import io.grpc.stub.StreamObserver;

/**
 * A {@link RequestListener} which forwards all its method calls to another {@link RequestListener}.
 * Subclasses should override one or more methods to modify the behavior of the backing implementation
 * as desired per the <a href="http://en.wikipedia.org/wiki/Decorator_pattern">decorator pattern</a>.
 */
abstract class ForwardingRequestListener<RequestT extends Message, ResponseT extends Message>
		extends ForwardingObject
		implements RequestListener<RequestT, ResponseT> {
	
	@Override
	protected abstract RequestListener<RequestT, ResponseT> delegate();
	
	@Override
	public void onNext(RequestT request) {
		delegate().onNext(request);
	}

	@Override
	public void onCompleted() {
		delegate().onCompleted();
	}
	
	@Override
	public StreamObserver<ResponseT> respond() {
		return delegate().respond();
	}

}