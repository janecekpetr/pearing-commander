package cz.michalbures.commander.service;

import static com.google.common.collect.MoreCollectors.onlyElement;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;

import cz.michalbures.commander.FileServiceGrpc;
import cz.michalbures.commander.FileServiceGrpc.FileServiceStub;
import cz.michalbures.commander.Pearing.DirectoryListing;
import cz.michalbures.commander.Pearing.File;
import cz.michalbures.commander.Pearing.File.FileType;
import cz.michalbures.commander.Pearing.FileTransfer;
import cz.michalbures.commander.Pearing.FileTransfer.Request.TransferType;
import cz.michalbures.commander.Pearing.FileTransfer.Response;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.jimfs.Configuration;
import com.google.common.jimfs.Jimfs;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import io.grpc.Status;
import io.grpc.Status.Code;
import io.grpc.stub.StreamObserver;
import io.grpc.testing.GrpcServerRule;
import io.grpc.testing.StreamRecorder;

@RunWith(Parameterized.class)
@SuppressWarnings("deprecation")	// we know it's going away
public class FileServiceTest {
	
	private static final String DOES_NOT_EXIST = "does-not-exist";
	private static final String DIRECTORY_NAME = "directory";
	private static final String FILE_NAME = "file";
	
	@Parameters
	public static ImmutableList<Configuration> fileSystemConfigurations() {
	    return ImmutableList.of(Configuration.unix(), Configuration.windows(), Configuration.osX());
	}
	
	@Rule
	public final Timeout timeout = Timeout.seconds(1);
	@Rule
	public final GrpcServerRule grpcServerRule = new GrpcServerRule().directExecutor();

	private final StreamRecorder<DirectoryListing.Response> responseRecorder = StreamRecorder.create();
	
	private final FileSystem fileSystem;
	private FileServiceStub fileService;
	private final String destinationFile;
	
	public FileServiceTest(Configuration fileSystemConfiguration) {
		this.fileSystem = Jimfs.newFileSystem(fileSystemConfiguration);
		this.destinationFile = Iterables.getOnlyElement(fileSystem.getRootDirectories())
			.resolve("destination-file")
			.toString();
	}

	@Before
	public void before() {
		grpcServerRule.getServiceRegistry().addService(new FileService(fileSystem));
		fileService = FileServiceGrpc.newStub(grpcServerRule.getChannel());
	}
	
	@Test
	public void testRequestEmptyDirectory_emptyResponse() throws Exception {
		// GIVEN
		Files.createDirectory(fileSystem.getPath(DIRECTORY_NAME));
		
		// WHEN
		requestDirectory(DIRECTORY_NAME);
		
		// THEN
		responseRecorder.awaitCompletion();
		DirectoryListing.Response onlyResponse = Iterables.getOnlyElement(responseRecorder.getValues());
		assertThat(onlyResponse.getDirectoryPath()).isNotEmpty();
		assertThat(onlyResponse.getFileList()).isEmpty();
	}

	@Test
	public void testRequestWorkingDir() throws Exception {
		// GIVEN
		Files.createDirectory(fileSystem.getPath(DIRECTORY_NAME));
		Files.createFile(fileSystem.getPath(FILE_NAME));
		
		// WHEN
		requestDirectory(".");

		// THEN
		responseRecorder.awaitCompletion();
		assertThat(responseRecorder.getValues()).hasSize(1);
		
		Map<FileType, File> filesByType = responseRecorder.getValues()
			.stream()
			.flatMap(directoryListingResponse -> directoryListingResponse.getFileList().stream())
			.collect(groupingBy(file -> file.getFileType(), onlyElement()));
		assertThat(filesByType.get(FileType.DIRECTORY).getName()).isEqualTo(DIRECTORY_NAME);
		assertThat(filesByType.get(FileType.FILE).getName()).isEqualTo(FILE_NAME);
	}
	
	@Test
	public void testRequestSymlinks() throws Exception {
		// GIVEN
		Files.createSymbolicLink(fileSystem.getPath("link-nowhere"), fileSystem.getPath(DOES_NOT_EXIST));
		
		Files.createSymbolicLink(fileSystem.getPath("link-file"), fileSystem.getPath(FILE_NAME));
		Files.createFile(fileSystem.getPath(FILE_NAME));
		
		Files.createSymbolicLink(fileSystem.getPath("link-directory"), fileSystem.getPath(DIRECTORY_NAME));
		Files.createDirectory(fileSystem.getPath(DIRECTORY_NAME));
		
		// WHEN
		requestDirectory(".");

		// THEN
		responseRecorder.awaitCompletion();
		assertThat(responseRecorder.getValues()).hasSize(1);
		
		Map<FileType, List<File>> filesByType = responseRecorder.getValues()
			.stream()
			.flatMap(directoryListingResponse -> directoryListingResponse.getFileList().stream())
			.collect(groupingBy(file -> file.getFileType()));
		List<String> symlinks = filesByType
			.get(FileType.SYMBOLIC_LINK)
			.stream()
			.map(file -> file.getName())
			.collect(toList());
		assertThat(symlinks).hasSize(3);
		assertThat(symlinks).contains("link-nowhere", "link-file", "link-directory");
	}
	
	@Test
	public void testMalformedRequest_invalidArgumentCode() throws Exception {
		requestDirectory("\0");
		assertErrorCode(Code.INVALID_ARGUMENT);
	}
	
	@Test
	public void testNonexistentDirectory_notFoundErrorCode() throws Exception {
		requestDirectory(DOES_NOT_EXIST);
		assertErrorCode(Code.NOT_FOUND);
	}
	
	@Test
	public void testRequestFile_failedPreconditionErrorCode() throws Exception {
		Files.createFile(fileSystem.getPath(FILE_NAME));
		
		requestDirectory(FILE_NAME);

		assertErrorCode(Code.FAILED_PRECONDITION);
	}
	
	@Ignore("Jimfs does not implement file permissions in a useful way.")
	@Test
	public void testRequestRestrictedFile_permissionDeniedErrorCode() throws Exception {
		Files.createDirectory(fileSystem.getPath(DIRECTORY_NAME));
		Files.setPosixFilePermissions(fileSystem.getPath(DIRECTORY_NAME), EnumSet.noneOf(PosixFilePermission.class));
		
		requestDirectory(DIRECTORY_NAME);

		assertErrorCode(Code.PERMISSION_DENIED);
	}
	
	@Test
	public void testMove() throws Exception {
		Files.createFile(fileSystem.getPath(FILE_NAME));

		StreamRecorder<Response> streamRecorder = StreamRecorder.create();
		StreamObserver<FileTransfer.Request> transferFilesCall = fileService.transferFiles(streamRecorder);
		transferFilesCall.onNext(FileTransfer.Request.newBuilder()
			.setTransferType(TransferType.MOVE)
			.addSource(FILE_NAME)
			.setDestination(destinationFile)
			.build());
		transferFilesCall.onCompleted();
		
		streamRecorder.awaitCompletion();
		assertThat(fileSystem.getPath(FILE_NAME)).doesNotExist();
		assertThat(fileSystem.getPath(destinationFile)).exists();
	}
	
	@Test
	public void testCopy() throws Exception {
		Files.createFile(fileSystem.getPath(FILE_NAME));

		StreamRecorder<Response> streamRecorder = StreamRecorder.create();
		StreamObserver<FileTransfer.Request> transferFilesCall = fileService.transferFiles(streamRecorder);
		transferFilesCall.onNext(FileTransfer.Request.newBuilder()
			.setTransferType(TransferType.COPY)
			.addSource(FILE_NAME)
			.setDestination(destinationFile)
			.build());
		transferFilesCall.onCompleted();
		
		streamRecorder.awaitCompletion();
		assertThat(fileSystem.getPath(FILE_NAME)).exists();
		assertThat(fileSystem.getPath(destinationFile)).exists();
	}
	
	private void assertErrorCode(Code code) throws Exception {
		responseRecorder.awaitCompletion();
		Throwable error = responseRecorder.getError();
		assertThat(error).isNotNull();
		assertThat(Status.fromThrowable(error).getCode()).isEqualTo(code);
	}
	
	private void requestDirectory(String directoryPath) {
		StreamObserver<DirectoryListing.Request> listDirectoryCall = fileService.listDirectory(responseRecorder);
		listDirectoryCall.onNext(createDirectoryRequest(directoryPath));
		listDirectoryCall.onCompleted();
	}
	
	private static DirectoryListing.Request createDirectoryRequest(String directoryPath) {
		return DirectoryListing.Request.newBuilder()
			.setDirectoryPath(directoryPath)
			.build();
	}
}