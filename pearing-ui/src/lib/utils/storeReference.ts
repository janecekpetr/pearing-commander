import {Store} from 'redux'
let reduxStore:Store<{}>;


export function setStore(store):void
{
	reduxStore = store;
}

export function getStore():Store<{}>
{
	return reduxStore;
}

export function getState():Object
{
	return reduxStore.getState();
}

export function dispatch(action):void
{
	reduxStore.dispatch(action);
}