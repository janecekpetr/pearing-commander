let webpack = require('webpack');
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let CopyWebpackPlugin = require('copy-webpack-plugin');
let WriteFilePlugin = require('write-file-webpack-plugin');
let nodeExternals = require('webpack-node-externals');


const PROJECT_NAME = 'pearing-ui';


let path = require('path');


let config = {
	devtool: 'source-map',
	context: path.join(__dirname, 'src'),
	entry: {
		"app-window/app-window": ['./app-window/__init__'],
		"main/main": ['./main/__init__']
	},
	target: "electron",
	output: {
		path: path.join(__dirname, 'app'),
		filename: '[name].bundle.js',
		devtoolModuleFilenameTemplate: '[absolute-resource-path]',
	},

	devServer: {
		outputPath: path.join(__dirname, 'app'),
		contentBase: './app',
		hot: false,
		port: 8001
	},

	module: {
		loaders: [
			{
				test: /\.css$/,
				// 				loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]&sourceMap!postcss-loader')
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader'),
				//				loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=' + PROJECT_NAME + '_[name]__[local]___[hash:base64:5]&sourceMap!less-loader'),

				include: /node_modules/,
			},
			{
				test: /\.js$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.css$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader'),
				exclude: /node_modules/,
			},
			{
				test: /\.less$/,
				loader: ExtractTextPlugin.extract('style-loader', 'css-loader?modules&importLoaders=1&localIdentName=' + PROJECT_NAME + '_[name]__[local]___[hash:base64:5]&sourceMap!less-loader')
			},
			{
				test: /\.json$/,
				loader: 'json-loader'
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2|png|svg|jpg)$/,
				loader: 'file-loader?name=/assets/[name].[ext]'
			},
			{
				test: /\.ts(x?)$/,
				exclude: /node_modules/,
				loader: 'babel-loader!ts-loader'
			},
		]
	},

	externals: [
		nodeExternals({
			// load non-javascript files with extensions 
			whitelist: [/\.(?!(?:jsx?|json)$).{1,5}$/i],
		})
	]
	,

	resolve: {
		modulesDirectories: [
			'src',
			'node_modules'
		],
		extensions: ['', '.json', '.js', '.css', '.ts', '.tsx']
	},

	plugins: [
		new CopyWebpackPlugin([
			{
				from: 'app-window/index.html',
				to: 'app-window/'
			}
		]),
		new WriteFilePlugin({
			test: /^(?!.*\.hot-update\.).*$/ // Don't write hot-update files
		}),
		new ExtractTextPlugin('[name].style.css', { allChunks: true }),
		new webpack.DefinePlugin({}),

	],

};


module.exports = config;