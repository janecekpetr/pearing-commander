import { app } from 'electron';
import { platform } from 'process';

export default function configureElectron()
{

	app.on('window-all-closed', function ()
	{
			if (process.platform !== 'darwin')
			{
				app.quit();
			}
	});

}