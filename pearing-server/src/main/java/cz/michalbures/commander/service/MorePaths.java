package cz.michalbures.commander.service;

import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;

import io.grpc.Status;

class MorePaths {
	
	private static final Status INVALID_PATH = Status.INVALID_ARGUMENT
		.withDescription("The provided path is malformed:");
	private static final Status FILE_NOT_FOUND = Status.NOT_FOUND
		.withDescription("The specified path does not exist:");
	private static final Status PERMISSION_DENIED = Status.PERMISSION_DENIED
		.withDescription("Not enough permissions to read the specified directory.");
	
	private MorePaths() {
		// a static utility class
	}
	
	public static Path parsePath(FileSystem fileSystem, String requestedPathString) {
		try {
			return fileSystem.getPath(requestedPathString);
		} catch (InvalidPathException e) {
			throw INVALID_PATH.augmentDescription(requestedPathString).withCause(e).asRuntimeException();
		}
	}
	
	public static Path checkAccessiblePath(Path requestedPath) {
		Status pathAccessStatus = getPathAccessStatus(requestedPath);
		if (pathAccessStatus.isOk()) {
			return requestedPath;
		}
		
		throw pathAccessStatus.asRuntimeException();
	}
	
	public static Status getPathAccessStatus(Path path) {
		if (Files.notExists(path)) {
			return FILE_NOT_FOUND.augmentDescription(path.toString());
		}
		if (!Files.isReadable(path)) {
			return PERMISSION_DENIED.augmentDescription(path.toString());
		}
		return Status.OK;
	}

}