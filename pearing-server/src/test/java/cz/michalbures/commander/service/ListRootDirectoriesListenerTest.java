package cz.michalbures.commander.service;

import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.util.concurrent.TimeUnit;

import cz.michalbures.commander.FileServiceGrpc;
import cz.michalbures.commander.FileServiceGrpc.FileServiceStub;
import cz.michalbures.commander.Pearing.ListRootDirectoriesRequest;
import cz.michalbures.commander.Pearing.RootDirectory;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.Timeout;

import io.grpc.stub.StreamObserver;
import io.grpc.testing.GrpcServerRule;
import io.grpc.testing.StreamRecorder;

public class ListRootDirectoriesListenerTest {
	
	@Rule
	public final Timeout timeout = new Timeout(2, TimeUnit.SECONDS);
	@Rule
	public final GrpcServerRule grpcServerRule = new GrpcServerRule().directExecutor();

	private final StreamRecorder<RootDirectory> responseRecorder = StreamRecorder.create();
	
	private final FileSystem fileSystem = FileSystems.getDefault();
	private FileServiceStub fileService;

	@Before
	public void before() {
		grpcServerRule.getServiceRegistry()
			.addService(new FileService(fileSystem));
		fileService = FileServiceGrpc.newStub(grpcServerRule.getChannel());
	}
	
	@Test
	public void test() throws Exception {
		StreamObserver<ListRootDirectoriesRequest> listRootDirectories = fileService.listRootDirectories(responseRecorder);
		listRootDirectories.onNext(ListRootDirectoriesRequest.getDefaultInstance());
		listRootDirectories.onCompleted();
	}

}