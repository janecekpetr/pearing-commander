import * as React from 'react';
import { Provider, connect } from 'react-redux';
import './DevTools';
import configureStore from './configureStore';
import { HashRouter as Router, Route } from 'react-router-dom'


const store = configureStore();


import './defaultStyles.css';
import 'normalize.css/normalize.css';
import 'react-virtualized/styles.css';
import '@blueprintjs/core/dist/blueprint.css';


import { DirListSimpleController } from 'modules/dir-list/components/DirListSimpleController';
import DirList from 'modules/dir-list/DirList';




export default () => {
	return (
		<Provider store={store}>
			<Router>
				<div style={{ display: 'flex', flexDirection: 'column', flex: '1' }}>
					<div><DirListSimpleController /></div>
					<DirList />
				</div>
			</Router>
		</Provider>
	);
}


