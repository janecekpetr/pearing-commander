package cz.michalbures.commander.rpc.request;

import static com.google.common.base.Preconditions.checkNotNull;

import com.google.protobuf.Message;

import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;

class ErrorHandlingRequestListener<RequestT extends Message, ResponseT extends Message>
		extends ForwardingRequestListener<RequestT, ResponseT>
		implements StreamObserver<RequestT> {

	private final RequestListener<RequestT, ResponseT> delegate;

	public static <RequestT extends Message, ResponseT extends Message>
		ErrorHandlingRequestListener<RequestT, ResponseT> wrap(RequestListener<RequestT, ResponseT> requestListener) {
		return new ErrorHandlingRequestListener<>(requestListener);
	}

	private ErrorHandlingRequestListener(RequestListener<RequestT, ResponseT> requestListener) {
		this.delegate = checkNotNull(requestListener);
	}

	@Override
	protected RequestListener<RequestT, ResponseT> delegate() {
		return delegate;
	}
	
	@Override
	public void onNext(RequestT request) {
		try {
			delegate.onNext(request);
		} catch (StatusRuntimeException e) {
			replyWithErrorAndThrow(e);
		} catch (Throwable t) {
			replyWithErrorAndThrow(t);
		}
	}

	@Override
	public void onCompleted() {
		try {
			delegate.onCompleted();
		} catch (StatusRuntimeException e) {
			replyWithErrorAndThrow(e);
		} catch (Throwable t) {
			replyWithErrorAndThrow(t);
		}
	}
	
	@Override
	public void onError(Throwable t) {
		replyWithErrorAndThrow(t);
	}
	
	private void replyWithErrorAndThrow(Throwable t) {
		StatusRuntimeException errorStatus = Status.fromThrowable(t).asRuntimeException();
		replyWithErrorAndThrow(errorStatus);
	}
	
	private void replyWithErrorAndThrow(StatusRuntimeException e) {
		respond().onError(e);
		throw e;
	}

}