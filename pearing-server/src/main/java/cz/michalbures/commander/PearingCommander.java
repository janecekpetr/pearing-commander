package cz.michalbures.commander;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.beust.jcommander.JCommander;

import org.slf4j.bridge.SLF4JBridgeHandler;

public class PearingCommander {

	public static void main(String[] args) throws IOException {
		bridgeJavaUtilLoggingToSlf4j();
		
		PearingCommanderServerParams serverParams = new PearingCommanderServerParams();
		
		JCommander jCommander = JCommander.newBuilder()
			.programName("Pearing Commander Server")
			.addObject(serverParams)
			.build();
		jCommander.parse(args);
		
		PearingCommanderServer pearingCommanderServer = new PearingCommanderServer(serverParams.getPort());
		pearingCommanderServer.start();
	}
	
	private static void bridgeJavaUtilLoggingToSlf4j() {
		LogManager.getLogManager().reset();
		SLF4JBridgeHandler.removeHandlersForRootLogger();
		SLF4JBridgeHandler.install();
		Logger.getLogger("global")
			.setLevel(Level.FINEST);
	}
	
}