package cz.michalbures.commander.service;

import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.stream.Collectors.toList;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.List;

import cz.michalbures.commander.Pearing.File;
import cz.michalbures.commander.Pearing.FileTransfer;
import cz.michalbures.commander.Pearing.FileTransfer.Request.TransferType;
import cz.michalbures.commander.rpc.request.RequestListener;

import com.google.common.io.MoreFiles;

import io.grpc.Status;
import io.grpc.stub.StreamObserver;

class FileTransferListener implements RequestListener<FileTransfer.Request, FileTransfer.Response> {
	
	private static final Status INVALID_TRANSFER_TYPE = Status.UNIMPLEMENTED
		.withDescription("The requested transfer operation is invalid/unknown. "
				+ "Available operations: " + Arrays.toString(TransferType.values()));
	private static final Status WRITE_PERMISSION_DENIED = Status.PERMISSION_DENIED
		.withDescription("Not enough permissions to write to the specified directory:");
	private static final Status UNEXPECTED_ERROR = Status.UNAVAILABLE
		.withDescription("Something bad happened while reading directory contents.");

	private final FileSystem fileSystem;
	private final StreamObserver<FileTransfer.Response> responseObserver;

	public FileTransferListener(FileSystem fileSystem, StreamObserver<FileTransfer.Response> responseObserver) {
		this.fileSystem = fileSystem;
		this.responseObserver = checkNotNull(responseObserver);
	}

	@Override
	public StreamObserver<FileTransfer.Response> respond() {
		return responseObserver;
	}
	
	@Override
	public void onNext(FileTransfer.Request request) {
		TransferType transferType = request.getTransferType();
		List<Path> sources = request.getSourceList()
			.stream()
			.map(sourcePathString -> MorePaths.parsePath(fileSystem, sourcePathString))
			.peek(MorePaths::checkAccessiblePath)
			.collect(toList());
		File.Filter sourceFilter = request.getFilter();
		String requestedDestination = request.getDestination();
		Path destination = MorePaths.parsePath(fileSystem, requestedDestination);
		// TODO this won't work with '/' vs '\'
		boolean isRequestedDirectory = requestedDestination.endsWith(fileSystem.getSeparator());
		try {
			MoreFiles.createParentDirectories(destination);
		} catch (IOException e) {
			throw WRITE_PERMISSION_DENIED
				.augmentDescription(destination.toString())
				.withCause(e)
				.asRuntimeException();
		}
		
		transferFiles(transferType, sources, destination, isRequestedDirectory);
		
		respond().onCompleted();
	}

	private static void transferFiles(TransferType transferType, List<Path> sources, Path destination, boolean isRequestedDirectory) {
		try {
			for (Path source : sources) {
				Path finalDestination;
				if (isRequestedDirectory) {
					finalDestination = source.getParent()
						.resolve(destination)
						.resolve(source.getFileName());
				} else {
					finalDestination = source.resolve(destination);
				}
				switch (transferType) {
					case COPY: {
						Files.copy(source, finalDestination);
						break;
					}
					case MOVE: {
						Files.move(source, finalDestination);
						break;
					}
					default:
						throw INVALID_TRANSFER_TYPE.asRuntimeException();
				}
			}
		} catch (IOException e) {
			throw UNEXPECTED_ERROR.withCause(e).asRuntimeException();
		}
	}
	
	@Override
	public void onCompleted() {
		// no-op
	}
}