import {ElectronReduxCommSaga} from 'electron-redux-multi-window-comm';
import {SimpleWindowManagerSaga} from 'electron-simple-window-manager';


import DirListSaga from 'ducks/dir-list/DirListSaga';

const RootSaga = [DirListSaga];

export default [ElectronReduxCommSaga, SimpleWindowManagerSaga, ...RootSaga];