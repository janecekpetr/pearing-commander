package cz.michalbures.commander.rpc.request;

import com.google.protobuf.Message;

import io.grpc.stub.StreamObserver;

public interface RequestListener<RequestT extends Message, ResponseT extends Message> {
	public void onNext(RequestT request);
	public void onCompleted();
	public StreamObserver<ResponseT> respond();
}